# Ansible playbook for kubernetes resources deployment

This role just do double loops: over clusters and stacks inside each cluster :)

Requirements:

- ansible 2.7+
- kubectl
- helm
- kuberentes cluster with docker runtime and service-node-port-range=22-65535 option
- ready to use kubectl config context

Usage example:

```sh
ansible-playbook stack.yml -e clusters="['cluster1']" -e cluster_stack="['stack1','stack2']"
```
